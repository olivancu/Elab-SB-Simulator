__author__ = 'Olivier Van Cutsem'

from simulation_config import *
import os
from multiprocessing import Process, Queue
import sys


# Add necessary path to the system
SIMU_DIR = os.path.abspath(DIR_SB_SIMU_FOLDER + DIR_VE)
EMS_DIR = os.path.abspath(DIR_SB_SIMU_FOLDER + DIR_EMS)
sys.path.append(SIMU_DIR)
sys.path.append(EMS_DIR)

from openems.launch_ems import ems_launcher
from vMiddleware_manager import vmid_launcher
from bmsinterface.bms_restful_client import get_unit_middleware
from multiprocessing import Queue
from ems_analysis.main_analysis import EMS_ANALYZER
from frontend.data_plot import EMS_DISPLAY, EMS_REPORT
from ems_config import EMS_GUI_SELECT_TYPE

# Instantiate the processes for the simulator and the controller

"""
! BE SURE THAT "SIMULATION_GUI_CONNECTED" is set to FALSE in:
 - vengine.virtualization.conf_virtualization.py
 - openems.ems_config.py
"""

def run_simulation():
    """
    Initializes the 2 main processes forming the SB-simulation, without any connection to the GUI
    :return:
    """
    graph_stream = Queue()

    p_ems = Process(target=ems_launcher, args=(UNIT_ID,
                                               CONTROLLER_TIME_STEP,
                                               SIMULATION_STARTING_DAY, SIMULATION_DATA_FOLDER, graph_stream))
    p_ems.start()

    list_vmid_id = get_unit_middleware(UNIT_ID)
    if len(list_vmid_id) > 1 or len(list_vmid_id) == 0:
        print("WARNING: list of middleware larger than 1. Took the first one.")

    SIMULATION_MID_ID = list_vmid_id[0]

    p_virt = Process(target=vmid_launcher, args=(SIMULATION_MID_ID,
                                                 SIMULATION_TIME_STEP, SIMULATION_DURATION, SIMULATION_STARTING_DAY,
                                                 None))
    p_virt.start()

    ### graph and analysis data collection

    data_collector = EMS_ANALYZER()
    while True:

        # Messages coming from the EMS interface
        msg_data = graph_stream.get(True)
        if msg_data is not None:
            try:
                type_data = msg_data[EMS_ANALYZER.MSG_TYPE_KEY]

                if type_data == EMS_ANALYZER.MSG_TYPE_ADD:  # new data to add
                    data_collector.analyse_data(msg_data[EMS_ANALYZER.MSG_CONTENT_TIME],msg_data[EMS_ANALYZER.MSG_CONTENT_DATA])
                elif type_data == EMS_ANALYZER.MSG_TYPE_PLOT:  # end of the simu: show data
                    report_ems = EMS_REPORT(data_collector).format_report()
                    msg_report = "Simulation analysis: \n\n{0}".format(report_ems)
                    print(msg_report)
                    EMS_DISPLAY(data_collector).show_data(EMS_GUI_SELECT_TYPE['comfort'], EMS_GUI_SELECT_TYPE['energy'])
            except KeyError:
                print("Wrong data format received from the OpenEMS. Please check you set SIMULATION_GUI_CONNECTED to false in both files. ")

if __name__ == '__main__':

    run_simulation()
