__author__ = 'Olivier Van Cutsem'

from openems.ems_analysis.main_analysis import EMS_ANALYZER
import matplotlib.pyplot as plt
from openems.building_data_management.category_management.category_config import *
from openems.ems_config import *
from simulation_config import SIMULATION_SAVE_AS_CSV, DIR_DATA_OUTPUT
import numpy as np
import copy
import math
import csv
import matplotlib

matplotlib.rcParams.update({'font.size': 14})

EMS_GUI_AGGREGATE_PLOT_LIMIT = 20


class EMS_DISPLAY:

    # Mapping the type of plot with the function to call
    mapping_plot_method_comfort = {0: 'show_comfort_individual_data', 1: 'show_comfort_per_quantity_data', 2: 'show_comfort_per_room_data'}
    mapping_plot_method_energy = {0: 'show_energy_individual_data', 1: 'show_energy_per_type_data', 2: 'show_energy_aggregated_data'}

    def __init__(self, stored_data):
        self.stored_data = stored_data

    ### Showing data

    def show_data(self, type_comfort_plot=0, type_energy_plot=0):
        """
        Plot the accumulated data
        :return:
        """

        if type(type_comfort_plot) is not list:
            type_comfort_plot = [type_comfort_plot]
        if type(type_energy_plot) is not list:
            type_energy_plot = [type_energy_plot]

        # Energy data plot
        for type_plot in type_energy_plot:
            getattr(self, EMS_DISPLAY.mapping_plot_method_energy[type_plot])()

        # Comfort data plot
        for type_plot in type_comfort_plot:
            getattr(self, EMS_DISPLAY.mapping_plot_method_comfort[type_plot])()

        plt.show()

    @staticmethod
    def adapt_plot_step_graph(x, y):
        x_adapted = x + [x[-1] + (x[-1] - x[-2])]
        y_adapted = [y[0]] + y
        return x_adapted, y_adapted

            ####################################################################################
            ###########################       COMFORT PLOTs         ############################
            ####################################################################################

    def show_comfort_per_room_data(self):
        """
        Plot the accumulated data aggregated per room
        :return:
        """
        pass #TODO

    def show_comfort_individual_data(self):
        """
        Plot the accumulated data as individual graphs
        :return:
        """

        # Show the temperature and luminosity
        room_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT]
        for r_name in room_list.keys():

            for comf_type in room_list[r_name]:
                _data_name = r_name+"_"+comf_type
                fig = plt.figure()
                ax = fig.add_subplot(111)
                x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in room_list[r_name][comf_type]],
                                                                [v for (t, v) in room_list[r_name][comf_type]])
                ax.plot(x_values, y_values)
                ax.grid(True)
                plt.title(_data_name)
                if SIMULATION_SAVE_AS_CSV:
                    save_data_as_csv([t/3600.0 for (t, v) in room_list[r_name][comf_type]],
                                     [v for (t, v) in room_list[r_name][comf_type]], _data_name)

        # Show the price of electricity
        env_data = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL]
        for env_data_type in env_data.keys():

            _data_name = "env_data_" + env_data_type
            fig = plt.figure()
            ax = fig.add_subplot(111)
            x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in env_data[env_data_type]],
                                                            [v for (t, v) in env_data[env_data_type]])
            ax.plot(x_values, y_values, drawstyle='steps')
            ax.grid(True)
            plt.title(env_data_type)
            if SIMULATION_SAVE_AS_CSV:
                save_data_as_csv([t/3600.0 for (t, v) in env_data[env_data_type]],
                                 [v for (t, v) in env_data[env_data_type]], _data_name)

    def show_comfort_per_quantity_data(self):
        """
        Plot the accumulated data aggregated per type of data
        :return:
        """

        room_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_COMFORT]
        constraint_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_CONSTRAINTS]
        ref_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_REF_VAL]
        nb_rooms = len(room_list.keys())

        # re-organize the data per type
        data_per_quantity = dict()
        for r_name in room_list.keys():  # The sensed data
            for comf_type in room_list[r_name]:
                if comf_type not in data_per_quantity:
                    data_per_quantity[comf_type] = dict()
                data_per_quantity[comf_type][r_name] = room_list[r_name][comf_type]

        const_per_quantity = dict()
        for r_name in constraint_list.keys():  # The constraint data
            for comf_type in constraint_list[r_name]:
                if comf_type not in const_per_quantity.keys():
                    const_per_quantity[comf_type] = dict()
                if r_name not in const_per_quantity[comf_type].keys():
                    const_per_quantity[comf_type][r_name] = dict()

                const_per_quantity[comf_type][r_name]['min'] = constraint_list[r_name][comf_type]['min']
                const_per_quantity[comf_type][r_name]['max'] = constraint_list[r_name][comf_type]['max']

        ref_per_quantity = dict()
        for r_name in ref_list.keys():  # The constraint data
            for comf_type in ref_list[r_name]:
                if comf_type not in ref_per_quantity.keys():
                    ref_per_quantity[comf_type] = dict()
                if r_name not in ref_per_quantity[comf_type].keys():
                    ref_per_quantity[comf_type][r_name] = []

                ref_per_quantity[comf_type][r_name] = ref_list[r_name][comf_type]

        # Show the results
        if nb_rooms < EMS_GUI_AGGREGATE_PLOT_LIMIT:  # everything on the same graph
            for comf_type, room_dict in data_per_quantity.items():
                fig = plt.figure(figsize=(18, 12))
                ax = fig.add_subplot(111)
                for r_name, r_data in room_dict.items():
                    # Comfort value as measured
                    _data_name = comf_type+"_val_"+r_name
                    x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in r_data],
                                                                    [v for (t, v) in r_data])
                    plot_obj, = ax.plot(x_values, y_values, drawstyle='steps', linewidth=1.5)
                    plot_obj.set_label('temp@Room "{0}"'.format(r_name))
                    if SIMULATION_SAVE_AS_CSV:
                        save_data_as_csv([t/3600.0 for (t, v) in r_data], [v for (t, v) in r_data], _data_name)

                    if comf_type in const_per_quantity.keys() and r_name in const_per_quantity[comf_type].keys():
                        # Min bound
                        _data_name = comf_type+"_min_"+r_name
                        min_vect = const_per_quantity[comf_type][r_name]['min']
                        x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in min_vect],
                                                                        [v for (t, v) in min_vect])
                        plot_obj, = ax.plot(x_values, y_values, drawstyle='steps')
                        plot_obj.set_label('min@Room "{0}"'.format(r_name))
                        if SIMULATION_SAVE_AS_CSV:
                            save_data_as_csv([t/3600.0 for (t, v) in min_vect],
                                             [v for (t, v) in min_vect], _data_name)

                        # Max bound
                        _data_name = comf_type+"_max_"+r_name
                        max_vect = const_per_quantity[comf_type][r_name]['max']
                        x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in max_vect],
                                                                        [v for (t, v) in max_vect])
                        plot_obj, = ax.plot(x_values, y_values, drawstyle='steps')
                        plot_obj.set_label('max@Room "{0}"'.format(r_name))
                        if SIMULATION_SAVE_AS_CSV:
                            save_data_as_csv([t/3600.0 for (t, v) in max_vect],
                                             [v for (t, v) in max_vect], _data_name)

                    if comf_type in ref_per_quantity.keys() and r_name in ref_per_quantity[comf_type].keys():
                        # ref val
                        _data_name = comf_type+"_ref_"+r_name
                        ref_vect = ref_per_quantity[comf_type][r_name]
                        x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in ref_vect],
                                                                        [v for (t, v) in ref_vect])
                        plot_obj, = ax.plot(x_values, y_values, drawstyle='steps', linestyle='dashed')
                        plot_obj.set_label('ref@Room "{0}"'.format(r_name))
                        if SIMULATION_SAVE_AS_CSV:
                            save_data_as_csv([t/3600.0 for (t, v) in ref_vect],
                                             [v for (t, v) in ref_vect], _data_name)

                ax.legend(loc='lower right')
                ax.grid(True)
                plt.xlabel("time (h)")
                plt.ylabel("Temperature ($^\circ C$)")

                ### Show and Save figure

                ax.grid('on')
                fig.savefig(DIR_DATA_OUTPUT+'comfort_%s_data.png' % comf_type, format='png', dpi=300)
                fig.savefig(DIR_DATA_OUTPUT+'comfort_%s_data.eps' % comf_type, format='eps', dpi=300)


        else:
            pass  #TODO

            ####################################################################################
            ###########################       ENERGY PLOTs         #############################
            ####################################################################################

    def show_energy_individual_data(self):
        """
        Plot the accumulated data aggregated per room
        :return:
        """

        # Print all the load consumption, storage energy and generation production
        loads_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_LOAD]
        for l_name in loads_list.keys():
            _data_name = "power_load_" + l_name
            fig = plt.figure()
            ax = fig.add_subplot(111)
            x_values = [t/3600.0 for (t, v) in loads_list[l_name]]
            y_values = [v for (t, v) in loads_list[l_name]]
            ax.plot(x_values, y_values)
            ax.grid(True)
            plt.title(_data_name)
            if SIMULATION_SAVE_AS_CSV:
                save_data_as_csv(x_values, y_values, _data_name)

        generation_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_GENERATION]
        for g_name in generation_list.keys():
            _data_name = "power_gen_" + g_name
            fig = plt.figure()
            ax = fig.add_subplot(111)
            x_values = [t/3600.0 for (t, v) in generation_list[g_name]]
            y_values = [v for (t, v) in generation_list[g_name]]
            ax.plot([t/3600.0 for (t, v) in generation_list[g_name]],
                    [v for (t, v) in generation_list[g_name]])
            ax.grid(True)
            plt.title(_data_name)
            if SIMULATION_SAVE_AS_CSV:
                save_data_as_csv(x_values, y_values, _data_name)

        storage_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_soc]
        for s_name in storage_list.keys():
            _data_name = "energy_stor_" + s_name
            fig = plt.figure()
            ax = fig.add_subplot(111)
            x_values = [t/3600.0 for (t, v) in storage_list[s_name]]
            y_values = [v for (t, v) in storage_list[s_name]]
            ax.plot(x_values, y_values)
            ax.grid(True)
            plt.title(_data_name)
            if SIMULATION_SAVE_AS_CSV:
                save_data_as_csv(x_values, y_values, _data_name)

    def show_energy_aggregated_data(self):
        """
        Plot the accumulated data aggregated per room
        :return:
        """

        p_acc = {'Load': [], 'Gen': [], 'Stor': []}
        e_acc = {'Stor': []}

        # Print all the load consumption, storage energy and generation production
        loads_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_LOAD]
        for l_name in loads_list.keys():
            if p_acc['Load'] == []:
                p_acc['Load'] = copy.deepcopy(loads_list[l_name])
            else:
                for i in range(0, len(loads_list[l_name])):
                    p_acc['Load'][i] = (p_acc['Load'][i][0], p_acc['Load'][i][1]+loads_list[l_name][i][1])
            print p_acc

        generation_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_GENERATION]
        for g_name in generation_list.keys():
            if p_acc['Gen'] == []:
                p_acc['Gen'] = copy.deepcopy(generation_list[g_name])
            else:
                for i in range(0, len(generation_list[g_name])):
                    p_acc['Gen'][i] = (p_acc['Gen'][i][0], p_acc['Gen'][i][1]+generation_list[g_name][i][1])

        storage_list_power = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_power]
        for s_name in storage_list_power.keys():
            if p_acc['Stor'] == []:
                p_acc['Stor'] = copy.deepcopy(storage_list_power[s_name])
            else:
                for i in range(0, len(storage_list_power[s_name])):
                    p_acc['Stor'][i] = (p_acc['Stor'][i][0], p_acc['Stor'][i][1]+storage_list_power[s_name][i][1])

        # Power graphs: L, G and S
        fig = plt.figure(figsize=(18, 12))
        ax = fig.add_subplot(311)
        for name, l_pts in p_acc.items():
            if len(l_pts) > 0:
                _data_name = "aggregated_power_"+name
                x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in l_pts],
                                                                [v for (t, v) in l_pts])
                sub_graph, = ax.plot(x_values, y_values, drawstyle='steps', linewidth=1.5)
                matplotlib.rcParams.update({'font.size': 14})
                sub_graph.set_label(name)
                if SIMULATION_SAVE_AS_CSV:
                    save_data_as_csv([t/3600.0 for (t, v) in l_pts], [v for (t, v) in l_pts], _data_name)
        ax.grid(True)
        ax.legend()
        ax.set_ylabel("Power (W)")


        # Power graphs: Grid
        (t1, p_acc_grid), (t2, p_sold_grid) = self.stored_data.get_grid_power_aggregated_timeseries()

        # Total electricity purchased
        if p_acc_grid != []:
            ax = fig.add_subplot(312)
            x_values, y_values = self.adapt_plot_step_graph(t1, p_acc_grid)
            sub_graph, = ax.plot([x_s/3600.0 for x_s in x_values], y_values, drawstyle='steps', linewidth=1.5)
            ax.set_ylabel("Power (W)")
            sub_graph.set_label("Power purchased")
            if SIMULATION_SAVE_AS_CSV:
                save_data_as_csv(t1, p_acc_grid, "purchased_grid_power")

            if p_sold_grid != []:
                x_values, y_values = self.adapt_plot_step_graph(t2, p_sold_grid)
                sub_graph, = ax.plot([x_s/3600.0 for x_s in x_values], y_values, drawstyle='steps', linewidth=1.5)

                sub_graph.set_label("Power sold")
                if SIMULATION_SAVE_AS_CSV:
                    save_data_as_csv(t2, p_sold_grid, "sold_grid_power")

            ax.grid(True)
            ax.legend(loc='lower left')

            # Electricity price
            if EMS_ENV_DATA_ELEC_PRICE in self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL].keys():
                ax2 = ax.twinx()
                price_list = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_EMS_LEVEL][EMS_ENV_DATA_ELEC_PRICE]

                x_values, y_values = self.adapt_plot_step_graph([t/3600.0 for (t, v) in price_list], [v for (t, v) in price_list])
                sub_graph, = ax2.plot(x_values, y_values, 'r--', drawstyle='steps')
                sub_graph.set_label("Buying price")
                ax2.set_ylabel('Electricity price ($/kWh)')
                ax2.legend(loc='lower right')
                if SIMULATION_SAVE_AS_CSV:
                    save_data_as_csv([t/3600.0 for (t, v) in price_list],
                                     [v for (t, v) in price_list], "buying_elec_price")


        storage_list_energy = self.stored_data.data_storage[EMS_ANALYZER.MSG_EMS_STATE_STRUCT_ENERGY][EMS_CATEGORY_ENTITY_STORAGE][EMS_ENERGY_soc]
        for s_name in storage_list_power.keys():
            if e_acc['Stor'] == []:
                e_acc['Stor'] = copy.deepcopy(storage_list_energy[s_name])
            else:
                for i in range(0, len(storage_list_energy[s_name])):
                    e_acc['Stor'][i][1] += storage_list_energy[s_name][i][1]

        # Energy graphs
        ax = fig.add_subplot(313)
        for name, l_pts in e_acc.items():
            x_values = [t/3600.0 for (t, v) in l_pts]
            y_values = [v for (t, v) in l_pts]
            sub_graph, = ax.plot(x_values, y_values, linewidth=1.5)
            ax.set_ylabel("SoC")
            ax.set_xlabel("time (h)")
            sub_graph.set_label(name)
            if SIMULATION_SAVE_AS_CSV:
                save_data_as_csv(x_values, y_values, "stored_energy_"+name)
        ax.legend()
        ax.grid(True)

        ### Save figure

        ### Show and Save figure
        ax.grid('on')
        fig.savefig(DIR_DATA_OUTPUT+'aggregated_elec_data.png', format='png')
        fig.savefig(DIR_DATA_OUTPUT+'aggregated_elec_data.eps', format='eps')

    def show_energy_per_type_data(self):
        """
        Plot the accumulated data aggregated per room
        :return:
        """

        # nb_plot = len(p_acc.keys())
        # n_rows = math.ceil(nb_plot / EMS_GUI_SUBPLOT_PLOTS_PER_ROW)
        # n_cols = EMS_GUI_SUBPLOT_PLOTS_PER_ROW
        # i = 1
        # for k, v in p_acc.items():
        #     ax = fig.add_subplot(n_rows, n_cols, i)
        #     ax.plot([t for (t, v) in storage_list[s_name]],
        #             [v for (t, v) in storage_list[s_name]])
        #     plt.title(s_name)
        #     i += 1
        pass  #TODO


class EMS_REPORT:

    EMS_REPORT_GRID_LEVEL = 'grid_level'
    EMS_REPORT_GRID_PURCHASED_ENERGY = 'grid_purchased_energy'
    EMS_REPORT_GRID_PURCHASED_COST = 'grid_purchased_cost'
    EMS_REPORT_GRID_PURCHASED_PAR = 'grid_purchased_par'
    EMS_REPORT_GRID_SOLD_ENERGY = 'grid_sold_energy'
    EMS_REPORT_GRID_SOLD_GAIN = 'grid_sold_gain'

    EMS_REPORT_INDIVIDUAL_ENTITY = 'individual_entity_level'
    EMS_REPORT_INDIVIDUAL_ENTITY_ENERGY = 'individual_energy'

    EMS_REPORT_COMFORT = 'comfort_level'
    EMS_REPORT_COMFORT_MEAN = 'comfort_mean'
    EMS_REPORT_COMFORT_MAX = 'comfort_maw'
    EMS_REPORT_COMFORT_MIN = 'comfort_min'
    EMS_REPORT_COMFORT_RMS_REF = 'rms_ref'

    def __init__(self, anal_obj):

        self.ems_analyser = anal_obj

    def generate_report(self):
        report_data = {}

        #### Grid level data ####
        report_data[self.EMS_REPORT_GRID_LEVEL] = dict()

        (t1, grid_purch), (t2, grid_sold) = self.ems_analyser.get_grid_power_aggregated_timeseries()
        t, price = self.ems_analyser.get_price_timeseries()
        price_sold = len(t) * [0] # todo

        # todo: don't assume that dt is fixed ..
        dt = t[1]-t[0]

        # Peak to average Ratio
        mean_grid_purch = np.mean(grid_purch)
        if mean_grid_purch > 0:
            par = np.max(grid_purch)/float(mean_grid_purch)
        else:
            par = 0

        report_data[self.EMS_REPORT_GRID_LEVEL][self.EMS_REPORT_GRID_PURCHASED_PAR] = par
        report_data[self.EMS_REPORT_GRID_LEVEL][self.EMS_REPORT_GRID_PURCHASED_ENERGY] = dt * sum(grid_purch)
        cost_elec = dt*sum(p*c for p, c in zip(grid_purch, price))
        report_data[self.EMS_REPORT_GRID_LEVEL][self.EMS_REPORT_GRID_PURCHASED_COST] = float(cost_elec)/(3600.0 * 1000.0)  # from seconds to hours

        report_data[self.EMS_REPORT_GRID_LEVEL][self.EMS_REPORT_GRID_SOLD_ENERGY] = -dt * sum(grid_sold)
        gain_sold = -dt/(3600*100)*sum(p*c for p, c in zip(grid_sold, price_sold))
        report_data[self.EMS_REPORT_GRID_LEVEL][self.EMS_REPORT_GRID_SOLD_GAIN] = float(gain_sold)/(3600.0 * 1000.0)  # from seconds to hours

        #### Individual data: Load, Batt and Gen ####
        report_data[self.EMS_REPORT_INDIVIDUAL_ENTITY] = dict()

        type_ent_list = [EMS_CATEGORY_ENTITY_LOAD, EMS_CATEGORY_ENTITY_STORAGE, EMS_CATEGORY_ENTITY_GENERATION]

        for e in type_ent_list:
            data_list = self.ems_analyser.get_indivual_power_timeseries(e)
            report_data[self.EMS_REPORT_INDIVIDUAL_ENTITY][e] = dict()
            for n in data_list:
                t, p = data_list[n]
                tot_energy = dt * sum([abs(v) for v in p])
                report_data[self.EMS_REPORT_INDIVIDUAL_ENTITY][e][n] = {self.EMS_REPORT_INDIVIDUAL_ENTITY_ENERGY: tot_energy}

        #### Room comfort ####
        report_data[self.EMS_REPORT_COMFORT] = dict()
        report_data[self.EMS_REPORT_COMFORT][EMS_COMFORT_temperature] = dict()
        report_comf = report_data[self.EMS_REPORT_COMFORT][EMS_COMFORT_temperature]

        list_data_room = self.ems_analyser.get_rooms_comfort_values_timeseries(EMS_COMFORT_temperature)
        for _r, _room_data in list_data_room.items():
            report_comf[_r] = dict()

            t, room_data = _room_data  # raw value
            report_comf[_r][self.EMS_REPORT_COMFORT_MEAN] = np.mean(room_data)
            report_comf[_r][self.EMS_REPORT_COMFORT_MAX] = np.max(room_data)
            report_comf[_r][self.EMS_REPORT_COMFORT_MIN] = np.min(room_data)

            t, ref = self.ems_analyser.get_rooms_comfort_reference_timeseries(_r, EMS_COMFORT_temperature)
            if ref != []:
                rms_ref = math.sqrt(sum([math.pow((v-ref_v), 2) for v, ref_v in zip(room_data, ref)])/len(room_data))
                report_comf[_r][self.EMS_REPORT_COMFORT_RMS_REF] = rms_ref

            #constr = self.ems_analyser.get_rooms_comfort_constraint_timeseries(_r, EMS_COMFORT_temperature)

        return report_data

    def format_report(self):
        formated_report = ""

        report = self.generate_report()

        # Grid level
        grid_level_report = report[self.EMS_REPORT_GRID_LEVEL]

        grid_level_format = "Grid-level analysis: \n"
        grid_level_format += " - Energy purchased: {0}kWh \n".format(round(grid_level_report[self.EMS_REPORT_GRID_PURCHASED_ENERGY]/(3600.0*1000.0), 3))
        grid_level_format += " - Electricity purchased bill: {0}$ \n".format(round(grid_level_report[self.EMS_REPORT_GRID_PURCHASED_COST], 2))
        grid_level_format += " - Peak-to-Average Ratio: {0} \n".format(round(grid_level_report[self.EMS_REPORT_GRID_PURCHASED_PAR], 3))
        grid_level_format += " - Energy sold: {0}kWh \n".format(round(grid_level_report[self.EMS_REPORT_GRID_SOLD_ENERGY]/(3600.0*1000.0), 3))
        grid_level_format += " - Electricity sold gain: {0}$ \n".format(round(grid_level_report[self.EMS_REPORT_GRID_SOLD_GAIN], 2))

        formated_report += grid_level_format

        # Individual level
        individual_level_report = report[self.EMS_REPORT_INDIVIDUAL_ENTITY]

        human_readible_type_ent_list = {EMS_CATEGORY_ENTITY_LOAD: 'Loads & Appliances',
                                        EMS_CATEGORY_ENTITY_STORAGE: 'Energy Storage Systems',
                                        EMS_CATEGORY_ENTITY_GENERATION: 'Local generators'}

        individual_format = "\nEntity-level analysis: \n"
        for e in individual_level_report.keys():
            individual_format += "| {0}: \n".format(human_readible_type_ent_list[e])
            for _n, _v in individual_level_report[e].items():
                energy = _v[self.EMS_REPORT_INDIVIDUAL_ENTITY_ENERGY]/(3600.0 * 1000.0)
                individual_format += " - {0} -> Total energy flow: {1}kWh\n".format(_n, round(energy, 3))

        formated_report += individual_format

        # Comfort-level report
        comfort_level_report = report[self.EMS_REPORT_COMFORT]

        human_readible_comfort_list = {EMS_COMFORT_temperature: 'Temperature'}
        human_readible_comfort_unit_list = {EMS_COMFORT_temperature: 'degC'}

        comfort_format = "\nComfort-level analysis: \n"

        for comf in comfort_level_report.keys():
            comfort_format += "- {0}:\n".format(human_readible_comfort_list[comf])
            comf_data = comfort_level_report[comf]
            for r, v in comf_data.items():
                comfort_format += "| Room '{0}':\n".format(r)
                mean_val = v[self.EMS_REPORT_COMFORT_MEAN]
                max_val = v[self.EMS_REPORT_COMFORT_MAX]
                min_val = v[self.EMS_REPORT_COMFORT_MIN]
                unit = human_readible_comfort_unit_list[comf]
                comfort_format += " - Mean: {0} {1}; Max: {2} {1}; Min: {3} {1} \n".format(round(mean_val, 2), unit,
                                                                                           round(max_val, 2),
                                                                                           round(min_val, 2))

                if self.EMS_REPORT_COMFORT_RMS_REF in v.keys():
                    rms_ref = v[self.EMS_REPORT_COMFORT_RMS_REF]
                    comfort_format += " - RMS-error to ref: {0}\n".format(round(rms_ref, 3))

        formated_report += comfort_format

        return formated_report


def save_data_as_csv(x_vect, y_vect, filename, folder=None):

    # Be sure this is a valid filename
    f_split = filename.split('.')
    if len(f_split) <= 1:  # there is no format
        filename += '.csv'  # Add ".csv" format at the end

    if folder is not None:  # TODO: create the folder if it doesn't exist
        pass  # be sure there is a "/" as the end ...
    else:
        folder = DIR_DATA_OUTPUT

    with open(folder+filename, "wb") as f:

        csv_writer = csv.writer(f, delimiter=',')
        csv_writer.writerows([[t, v] for (t, v) in zip(x_vect, y_vect)])
