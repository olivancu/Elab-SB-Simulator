
import sys, time, datetime
import json
from multiprocessing import Queue, Process
from PyQt4.QtCore import *

# Data collecting and plotting
from openems.ems_analysis.main_analysis import EMS_ANALYZER
from data_plot import EMS_DISPLAY, EMS_REPORT
from openems.ems_config import EMS_GUI_SELECT_TYPE

from simulation_config import *
import os

### Add path for process calling

GUI_LOG_SENDER_NAME = 'gui_sender_name'
GUI_LOG_SENDER_NAME_EMS = 'gui_sender_ems'
GUI_LOG_SENDER_NAME_VE = 'gui_sender_ve'

GUI_LOG_TYPE = 'gui_msg_type'
GUI_LOG_TYPE_INFO = 'gui_msg_type_info'  # Info to display
GUI_LOG_TYPE_DATA = 'gui_msg_type_data'  # Data for graph/analysis
GUI_LOG_TYPE_PROGRESSION = 'gui_msg_type_prog'  # Simulation progression
GUI_LOG_TYPE_TIMING = 'gui_msg_type_timing'  # various timing within the simulator

GUI_LOG_PAYLOAD = 'gui_msg_payload'

GUI_LOG_TIMESTAMP = 'gui_msg_timestamp'

class GUI_Interface(QObject):
    """
    This class connects the simulation message with the GUI.
    It strives to decouple the logic from the interface style
    """
    def __init__(self, gui, parent=None):
        super(self.__class__, self).__init__(parent)

        # Setup the worker object and the worker_thread.
        self.worker = InterfaceWorker()
        self.worker_thread = QThread()
        self.worker.moveToThread(self.worker_thread)
        self.worker_thread.start()

        self.gui = gui
        self.data_analyser = None

        # Make any cross object connections.
        self._connectSignals()

        #### SIMULATION purpose
        self._launched_process = []

    def _connectSignals(self):

        # Start simulation
        self.gui.run_btn.clicked.connect(self.worker._run_main_loop)
        self.gui.run_btn.clicked.connect(self.__start_simu)

        # Stop simulation
        self.gui.stop_btn.clicked.connect(self.forceWorkerQuit)
        self.gui.stop_btn.clicked.connect(self.__interrupt_simu)
        self.worker.msg_from_simu.connect(self.handle_simu_msg)

        self.gui.aboutToQuit.connect(self.forceWorkerQuit)

    def forceWorkerQuit(self):
        if self.worker_thread.isRunning():
            self.worker_thread.quit()
        # Signal-linked methods

    def __start_simu(self):

        # Clean the logs
        self.reset_log(self.gui.ems_log, msg="Running OpenEMS")
        self.reset_log(self.gui.ve_log, msg="Running Virtualization Engine")

        # Kill the previous processes:
        self.__interrupt_simu()

        # Prepare the data analyser object
        self.data_analyser = EMS_ANALYZER()

        # launch the processes
        self._launched_process = run_simulation(self.worker.queue)  # Launch the processes

    def __interrupt_simu(self):

        for p in self._launched_process:
            if p.is_alive():
                p.terminate()

        self._launched_process = []

        # Reset progress bar
        self.update_progression(None)

    @pyqtSlot(str)
    def handle_simu_msg(self, msg):

        if msg is None:
            return

        msg = json.loads(str(msg))

        msg_sender = msg[GUI_LOG_SENDER_NAME]
        msg_type = msg[GUI_LOG_TYPE]
        msg_payload = msg[GUI_LOG_PAYLOAD]
        msg_time_stamp = msg[GUI_LOG_TIMESTAMP]

        # Determine the action

        if msg_type == GUI_LOG_TYPE_INFO or msg_type == GUI_LOG_TYPE_TIMING:

            log = self.gui.ems_log
            if msg_sender == GUI_LOG_SENDER_NAME_VE:
                log = self.gui.ve_log

            self.display_log(log, msg_payload, msg_time_stamp)

        elif msg_type == GUI_LOG_TYPE_PROGRESSION:
            self.update_progression(msg_payload)
        elif msg_type == GUI_LOG_TYPE_TIMING:
            pass
        elif msg_type == GUI_LOG_TYPE_DATA:  # data for the EMS_ANALYZER
            type_data = msg_payload[EMS_ANALYZER.MSG_TYPE_KEY]

            if type_data == EMS_ANALYZER.MSG_TYPE_ADD:  # new data to add
                self.data_analyser.analyse_data(msg_payload[EMS_ANALYZER.MSG_CONTENT_TIME], msg_payload[EMS_ANALYZER.MSG_CONTENT_DATA])
            elif type_data == EMS_ANALYZER.MSG_TYPE_PLOT:  # end of the simu: show data and analyse them

                # Report of analysis
                report_ems = EMS_REPORT(self.data_analyser).format_report()
                msg_report = "Simulation analysis: \n\n{0}".format(report_ems)
                self.display_log(self.gui.ems_log, msg_report, time.time())

                with open("data/output/analysis_report.txt", "w") as text_file:
                    text_file.write(msg_report)

                # Plots
                EMS_DISPLAY(self.data_analyser).show_data(EMS_GUI_SELECT_TYPE['comfort'], EMS_GUI_SELECT_TYPE['energy'])

    def reset_log(self, log, msg=None):

        if msg is not None:
            log.setPlainText(msg)
        else:
            log.setPlainText("")

    def display_log(self, log, msg, time_msg):

        time_display = datetime.datetime.fromtimestamp(time_msg).strftime('%H:%M:%S')
        log.appendPlainText('{0} -  {1}'.format(time_display, msg))

    ### Progression of the simulation

    def update_progression(self, global_time):

        start_t = SIMULATION_STARTING_DAY * 24 * 60 *60
        dur = SIMULATION_DURATION

        if global_time is None:
            global_time = int(start_t)

        if type(global_time) is float or type(global_time) is int:
            percentage_prog = float(global_time-start_t)/float(dur)*100.0
            self.gui.progress_simu.setValue(percentage_prog)

class InterfaceWorker(QObject):
    """
    The class in charge of transferring message from processes to the QT GUI
    """

    msg_from_simu = pyqtSignal(str)

    def __init__(self, parent=None):

        super(InterfaceWorker, self).__init__(parent)

        self.queue = Queue()

    @pyqtSlot(str)
    def _run_main_loop(self):
        while True:

            msg = self.queue.get(True)
            self.msg_from_simu.emit(json.dumps(msg))


def run_simulation(gui_stream):
    """
    Initializes the 2 main processes forming the SB-simulation and returns the list of process objects
    :param gui_stream: a Queue object for sending data with the GUI
    :return:
    """

    SIMU_DIR = os.path.abspath(DIR_SB_SIMU_FOLDER + DIR_VE)
    EMS_DIR = os.path.abspath(DIR_SB_SIMU_FOLDER + DIR_EMS)
    sys.path.append(SIMU_DIR)
    sys.path.append(EMS_DIR)

    from openems.launch_ems import ems_launcher
    from vengine.virtualization.vMid_core.vMiddleware_manager import vmid_launcher
    from openems.bmsinterface.bms_restful_client import get_unit_middleware

    p_ems = Process(target=ems_launcher, args=(UNIT_ID,
                                               CONTROLLER_TIME_STEP,
                                               SIMULATION_STARTING_DAY, SIMULATION_DATA_FOLDER, gui_stream))
    p_ems.start()

    list_vmid_id = get_unit_middleware(UNIT_ID)
    if len(list_vmid_id) > 1 or len(list_vmid_id) == 0:
        print("WARNING: list of middleware larger than 1. Took the first one.")

    SIMULATION_MID_ID = list_vmid_id[0]

    p_virt = Process(target=vmid_launcher, args=(SIMULATION_MID_ID,
                                                 SIMULATION_TIME_STEP, SIMULATION_DURATION, SIMULATION_STARTING_DAY,
                                                 gui_stream))
    p_virt.start()

    return [p_ems, p_virt]


def format_log_msg(sender, type_m, payload, timestamp=None):

    ret = dict()

    ret[GUI_LOG_SENDER_NAME] = sender
    ret[GUI_LOG_TYPE] = type_m
    ret[GUI_LOG_PAYLOAD] = payload

    if timestamp is None:
        timestamp = time.time()

    ret[GUI_LOG_TIMESTAMP] = timestamp

    return ret
