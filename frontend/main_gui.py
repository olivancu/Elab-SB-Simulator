#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *


class EMS_QT_GUI(QApplication):
    """
    MAIN GUI frame
    """

    def __init__(self, argv, transFile=None):

        # Main QT application
        QApplication.__init__(self, argv)

        # Main Widget
        self.__mainWid = QWidget()

        # Title
        self.__mainWid.setWindowTitle("ELAB Smart-Building simulation project")

        ##### The layout of the program interface ####

        self.__mainWid.setGeometry(350, 100, 1500, 900)
        self.grid_layout = QGridLayout()

        coreLayout = QHBoxLayout()
        bottomLayout = QHBoxLayout()

        ### Main layout: two columns

        self.ems_col = QVBoxLayout()
        self.ve_col = QVBoxLayout()

        coreLayout.addLayout(self.ve_col)
        coreLayout.addLayout(self.ems_col)

        # EMS column:

        self.ems_log = QPlainTextEdit()
        self.ems_log.setReadOnly(True)
        self.ems_log.setStyleSheet("""
        .QPlainTextEdit {
            border: 2px solid white;
            background-color: rgb(50, 50, 50);
            color:white;
            font-size: 14px;
            }
        """)
        self.ems_log.appendPlainText("")

        self.ems_title = QLabel()
        self.ems_title.setText("OpenEMS")
        self.ems_title.setAlignment(Qt.AlignCenter)
        self.ems_title.setStyleSheet("""
        .QLabel {
            color: black;
            font-size: 20px;
            }
        """)

        self.ems_col.addWidget(self.ems_title)
        self.ems_col.addWidget(self.ems_log)

        # vEngine column:

        self.ve_log = QPlainTextEdit()
        self.ve_log.setReadOnly(True)
        self.ve_log.setStyleSheet("""
        .QPlainTextEdit {
            border: 2px solid white;
            background-color: rgb(50, 50, 50);
            color:white;
            font-size: 14px;
            }
        """)
        self.ve_log.appendPlainText("")

        self.ve_title = QLabel()
        self.ve_title.setText("Virtualization Engine")
        self.ve_title.setAlignment(Qt.AlignCenter)
        self.ve_title.setStyleSheet("""
        .QLabel {
            color: black;
            font-size: 20px;
            }
        """)

        self.ve_col.addWidget(self.ve_title)
        self.ve_col.addWidget(self.ve_log)

        # Progress, Run and stop

        self.progress_simu = QProgressBar()
        self.progress_simu.setGeometry(200, 80, 250, 20)

        # Run button
        self.run_btn = QPushButton("Run", self.__mainWid)

        # Stop simu
        self.stop_btn = QPushButton("Stop", self.__mainWid)

        bottomLayout.addWidget(self.run_btn)
        bottomLayout.addWidget(self.progress_simu)
        bottomLayout.addWidget(self.stop_btn)


        # Elements ordering
        self.grid_layout.addLayout(coreLayout, 1, 1)
        self.grid_layout.addLayout(bottomLayout, 2, 1)
        self.__mainWid.setLayout(self.grid_layout)

    def run(self):
        self.__mainWid.show()
        self.exec_()

