__author__ = 'Olivier Van Cutsem'

### Define relative paths for pointing to the various project folders

DIR_SB_SIMU_FOLDER = '/'

DIR_EMS = 'openems/'
DIR_VE = 'vengine/virtualization/vMid_core/'
DIR_DATA = 'data/minergie/'
DIR_DATA_OUTPUT = 'data/output/'

SIMULATION_DATA_FOLDER = DIR_SB_SIMU_FOLDER + DIR_DATA


                            #########################################
                            ######### Simulation parameters #########
                            #########################################

# Time of simulation
UNIT_ID = 11
SIMULATION_TIME_STEP = 1*60
CONTROLLER_TIME_STEP = 1*60
SIMULATION_DURATION = 1*24*60*60
SIMULATION_STARTING_DAY = 0


# Config for data saving
SIMULATION_SAVE_AS_CSV = True

# Type of Control logic
EMS_TYPE_CLASS = None
EMS_ENABLE_PLANNING_PHASE = False
