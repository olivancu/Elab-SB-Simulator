__author__ = 'Olivier Van Cutsem'

import sys

from frontend.main_gui import EMS_QT_GUI
from frontend.interface import GUI_Interface

if __name__ == '__main__':

    qt_appli = EMS_QT_GUI(sys.argv)  # The main Graphical User Interface
    gui_interface = GUI_Interface(qt_appli)  # The backend interface that updates the graphical content of the GUI
    qt_appli.run()
