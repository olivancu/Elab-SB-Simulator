from simulation_config import *
from multiprocessing import Process, Queue
import os
import sys, inspect

# Add OPENEMS and vENGINE in the PATH
list_folders = ["./openems", "./vengine/virtualization"]
for folder in list_folders:
    if folder not in sys.path:
        sys.path.append(os.path.abspath(folder))
        # sys.path.insert(0, folder)

from openems.launch_ems import ems_launcher
from vengine.virtualization.vMid_core.vMiddleware_manager import vmid_launcher
from openems.bmsinterface.bms_restful_client import get_unit_middleware


def spawn_smartbuilding_as_sg_entity(sb_id, sb_param, simulation_param=None):
    """"""

    simu_starting_day = SIMULATION_STARTING_DAY
    simu_timestep = CONTROLLER_TIME_STEP
    simu_duration = SIMULATION_DURATION

    # Read the parameters
    sb_unit = UNIT_ID
    if 'unit' in sb_param.keys():
        sb_unit = sb_param['unit']

    sb_grid_param = {'sb_id': sb_id}

    if simulation_param is not None:

        if "STARTING_DATE" in simulation_param.keys():
            simu_starting_day = simulation_param["STARTING_DATE"]
        if "TIME_STEP" in simulation_param.keys():
            simu_timestep = simulation_param["TIME_STEP"]
        if "DURATION" in simulation_param.keys():
            simu_duration = simulation_param["DURATION"]

    # --- Open EMS process

    print("LAUNCHING EMS sb#{}".format(sb_id))
    p_ems = Process(target=ems_launcher, args=(sb_unit,
                                               simu_timestep,
                                               simu_starting_day, SIMULATION_DATA_FOLDER, None,
                                               sb_grid_param))

    p_ems.start()

    # --- Virtualization Engine

    if simulation_param["DURATION"] > 0:
        print("LAUNCHING vENGINE sb#{}".format(sb_id))
        # Get the middleware ID corresponding to the UNIT
        list_vmid_id = get_unit_middleware(UNIT_ID)
        if len(list_vmid_id) > 1 or len(list_vmid_id) == 0:
            print("WARNING: list of middleware larger than 1. Took the first one.")

        SIMULATION_MID_ID = list_vmid_id[0]

        p_virt = Process(target=vmid_launcher, args=(SIMULATION_MID_ID,
                                                     simu_timestep, simu_duration, simu_starting_day,
                                                     None, sb_grid_param))
        p_virt.start()

    # EMS will receive the STOP signal -> stop the Virtualization Engine if it does not stop by itself
    print("WAITING FOR EMS sb#{}".format(sb_id))
    p_ems.join()

    if simulation_param["DURATION"] > 0:
        if p_virt.is_alive():
            print("WAITING FOR VENGINE sb#{}".format(sb_id))
            p_virt.terminate()


if __name__ == '__main__':

    id_sb = 0

    # Get data from the script call
    param = []
    if len(sys.argv) > 1:
        param = sys.argv[1]
        id_sb = param[0]

    # Wrap simulation data in a dict to pass them to the processes
    param_simu = {}
    param_simu["sb_id"] = id_sb

    spawn_smartbuilding_as_sg_entity(param_simu)
