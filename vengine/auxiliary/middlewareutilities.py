__author__ = 'Georgios Lilis'
import sys, os, json, inspect
import requests
import logging
from conf_general import CURRENT_IP
from conf_openbms import OPENBMS_IP, OPENBMS_PORT, APIsecKey

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
BMSDATA_STORE_PATH = cmd_folder + "/.." + "/virtualization/bms_requests/"
BMSDATA_STORE_ENABLE = True

def get_bms_api_data(type_api, data_api):
    """
    Call the BMS api
    """

    geturl = 'http://{0}:{1}/API/{2}/{3}'.format(OPENBMS_IP, OPENBMS_PORT, type_api, data_api)

    filename = "{0}_{1}.json".format(type_api, data_api)
    filename = filename.replace('/','_')

    if BMSDATA_STORE_ENABLE:
        try:
            with open(BMSDATA_STORE_PATH+filename, 'r') as infile:
                loaded_data = json.load(infile)
                return loaded_data, 200
        except:
            pass

    r = requests.get(geturl)
    data = None
    status_code = r.status_code

    if r.status_code == 200:  # Otherwise it means openBMS doesnt work
        data = r.json()

        if BMSDATA_STORE_ENABLE:
            with open(BMSDATA_STORE_PATH+filename, 'w') as outfile:
                json.dump(data, outfile)

    return data, status_code

def get_middleware_details(midid):
    """Returns the middleware dictionary for use by the daemons"""

    r_data, r_status_code = get_bms_api_data('middleware', midid)

    if r_status_code == 200: # Otherwise it means openBMS doesnt work
        middleware = r_data[0]['fields']
        if middleware['IP'] == '255.255.255.255':
            logging.debug('Reporting back the CURRENT_IP {0} to replace default 255.255.255.255'.format(CURRENT_IP))
            #set_middleware_ip(midid=midid, midIP=CURRENT_IP)
            middleware['IP'] = CURRENT_IP
        if middleware['IP'] == '127.0.0.1':
            logging.debug('The middleware address is configured as 127.0.0.1. Binding the socket to the default IP of openBMS {0}'.format(OPENBMS_IP))
        return middleware
    elif r_status_code == 404 and r_data:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()

def get_middleware_objects(midid):
    """Returns the connected objects to the middleware"""
    r_data, r_status_code = get_bms_api_data('middleware_objects', midid)

    if r_status_code == 200: # Otherwise it means openBMS doesnt work
        middlewareobjects = list()
        for midobj in r_data:
            middlewareobjects.append(midobj['fields'])
        return middlewareobjects
    elif r_status_code == 404 and r_data:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()

def get_middleware_refs(midid):
    """Returns the connected refs to the middleware"""
    r_data, r_status_code = get_bms_api_data('middleware_refs', midid)
    if r_status_code == 200: # Otherwise it means openBMS doesnt work
        return r_data
    elif r_status_code == 404 and r_data:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()

def set_middleware_ip(midid, midIP):
    """Report the IP back to openBMS for autoconfiguration"""
    posturl = 'http://{0}:{1}/API/middleware_reportip/{2}/?IP={3}&api_sec_key={4}'.format(OPENBMS_IP,OPENBMS_PORT, midid, midIP, APIsecKey)
    r = requests.post(posturl)
    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        return True
    elif r.status_code == 404 and r.content:  # Server replied but not found the midid
        logging.error('Failed to find in openBMS the middleware: {0}'.format(midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()